FROM nginx:alpine

# Set env 
env     DOCKER_BIN_DIR=/usr/local/bin \
        PROJECT_ROOT=/srv/git \
        SERVER_CONF=/etc/nginx/conf.d \
        STATIC_ROOT=/var/www/html 

# Copy files
# COPY src/bin $DOCKER_BIN_DIR/
# COPY src/nginx $SERVER_CONF/


# install git
# RUN apk --no-cache --update add \
#     git
# clone repository
# RUN git clone git://git.codemadness.org/stagit



# Install build dependencies
RUN apk --no-cache --update add \
    build-base \
    libgit2-dev \
    tree \
    musl

# WORKDIR /stagit
# RUN make && make install

# RUN chmod +x $DOCKER_BIN_DIR/entrypoint.sh

# WORKDIR /

# Temporarily copy from local files
COPY stagit /stagit
RUN cd /stagit && make && make install

COPY media/logo.png /
COPY src/bin $DOCKER_BIN_DIR/
COPY src/nginx $SERVER_CONF/

RUN mkdir $PROJECT_ROOT
# RUN mkdir $STATIC_ROOT
RUN mkdir -p /var/www/html
COPY src/config / 

WORKDIR /var/www/html
#WORKDIR /

# RUN adduser -D -g '' --no-create-home git

#USER git

EXPOSE 8080

# HEALTHCHECK CMD nginx -t &>/dev/null && wget -O o localhost:80 &>/dev/null \
#     || exit 1


ENTRYPOINT ["entrypoint.sh"]

CMD ["nginx", "-g", "daemon off;"]
