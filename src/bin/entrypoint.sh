#!/bin/sh -e
# exit immediately on error
#

echo "environment varibles: "
env


# copy style 
cp -f /usr/local/share/doc/stagit/style.css $STATIC_ROOT/style.css

# Check if repos are attached
# echo "in repo root $PROJECT_ROOT"
# ls $PROJECT_ROOT

echo "find git repos"
find $PROJECT_ROOT -maxdepth 2 -mindepth 2 -type d -name .git | cut -d '/' -f4

# Create static files
make -C / all

# Add cron setting
(crontab -l; echo "* * * * * make -C / all") | crontab -
#
# Start cron daemon
crond -b -l 8

# Move default nginx config
mv /etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf.bkp

# cd $STATIC_ROOT
exec "$@"


# Check if projects volume is attached


# for d in /$PROJECT_ROOT/; do
#     mdkir 
#     echo "$d"
# done



# generate dirs
#
# Run stagit
#
# Run stagit index
