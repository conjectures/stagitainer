

BLD_ARG	?= --build-arg PROJECT_ROOT=../testrepo
BLD_REPO ?= git-server
BLD_VER ?= latest

REPO_DIR= ../testrepo
MEDIA_DIR = media
SRV_DIR = htmlrepo


#TODO: check if REPO_DIR provided
PROJECTS=$(shell find $(REPO_DIR) -maxdepth 2 -mindepth 2 -type d -name .git | cut -d '/' -f3)
# PROJECTS=$(shell find $(REPO_DIR) -maxdepth 1 -mindepth 1 -type d )
# TODO check if SRV_DIR exists or ignore warning
STATICS=$(shell find $(SRV_DIR) -maxdepth 1 -mindepth 1 -type d)
# PROJECTS := $(shell )




build: Dockerfile
	docker build $(BLD_ARG) $(addprefix --tag $(BLD_REPO):, $(BLD_VER) $(git rev-parse --abbrev-ref HEAD)) .


rund: rm
	docker run -d -it --name devtest -p 8080:8080/tcp --mount type=bind,source="$(PWD)"/../testrepo/,target=/srv/git $(BLD_REPO)

rm: 
	-docker ps -aq | xargs docker rm


prune: 
	docker image prune -f

run: rm
	docker run --name devtest -p 8080:8080/tcp --mount type=bind,source="$(PWD)"/../testrepo/,target=/srv/git $(BLD_REPO)



# vpath $(SRV_DIR)/% $(dir $(REPO_DIR)/$(PROJECTS))
#
#vpath $(STATICS): $(REPO_DIR)/
# vpath $(addprefix $(REPO_DIR)/, $(PROJECTS)) $(SRV_DIR)
# vpath $(PROJECTS) $(SRV_DIR)


#test: $(addprefix $(REPO_DIR)/, $(PROJECTS))
test: $(SRV_DIR)/

$(SRV_DIR)/: $(addprefix $(SRV_DIR)/, $(PROJECTS)) $(SRV_DIR)/%.css $(SRV_DIR)/logo.png $(SRV_DIR)/favicon.png
	@echo checking projects
	@ #echo $@ - $^ 

	@ #link repos to main index
	mkdir -p $(SRV_DIR) && cd $(SRV_DIR) && stagit-index $(addprefix ../$(REPO_DIR)/, $(PROJECTS)) > index.html


$(SRV_DIR)/%:$(REPO_DIR)/%
#ifneq ("$(wildcard $^/.git)","")
	# echo "$(wildcard $^/.git)"
	mkdir -p $@

	-cd $^; \
		echo "git://git.example.com/$@" > url;

	@# ignore error is simpler than checking if .git exists for prereqs
	-cd $@; \
		stagit ../../$^; \
		ln -sf ../style.css style.css; \
		ln -sf ../logo.png logo.png; \
		ln -sf ../favicon.png favicon.png;
# endif

$(SRV_DIR)/%.css:
	-cp src/style/*.css $(SRV_DIR)/

$(SRV_DIR)/logo.png: $(MEDIA_DIR)/logo.png
	-cp media/logo.png $(SRV_DIR)/logo.png

$(SRV_DIR)/favicon.png: $(MEDIA_DIR)/favicon.png
	-cp media/favicon.png $(SRV_DIR)/favicon.png

clean:
	-rm -rv $(SRV_DIR)/*

# .PHONY: $(PROJECTS)
# $(STATICS) : 
# 	@echo executing statics rule
# 	@echo $@ - $^
# 
# $(addprefix $(REPO_DIR)/, $(PROJECTS)): 
# 	@# mkdir -p $(SRV_DIR)
# 	@echo executing project rule
# 	@echo $@ - $^
# 
# $(STATICS): $(REPO_DIR)/$(PROJECTS)
# 	@echo checking projects
# 	@echo $(PROJECTS) - $@ $^
# 
# trigger every time 
# .PHONY: force
# 
# $(PROJECTS): force
# 	@# echo $@ | cut -d '/' -f3' 
# 	@# REPO = $(shell cut -d '/' -f3- <<< $@)
# 	@ # echo $(SRV_DIR)/$(shell cut -d '/' -f3- <<< $@) | xargs mkdir -p
# 	@# echo $(SRV_DIR)/$@ | xargs mkdir -p && cd $(SRV_DIR)/$@
# 	mkdir -p $(SRV_DIR)/$@ && $(SRV_DIR)/$@
# 	$(PWD)
