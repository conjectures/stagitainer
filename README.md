# Containerised static git server

Containerised static git server using the [stagit](https://git.codemadness.org/stagit/) page generator.

## Design 

## Install

## Documentation

## Roadmap

- proof of concept
    - [x] nginx 
    - [ ] multi stage dockerfile
    - [ ] static build stagit binaries


- cicd friendly
    - [x] use makefile to rebuild static sites
    - [x] cronjob update with make

- git (separate container)
    - [ ] add git clone url
    - [ ] post-receive hook
    - [ ] ssh & authentication


- prod ready
    - [ ] tarball backups?
    - [ ] storage interface / CSI ([hetzner](https://github.com/hetznercloud/csi-driver))

    
- extras
    - [ ] golang implementation
    - [ ] md renderer
